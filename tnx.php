<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Survey (NL)</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
        .likert ul
        {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        .likert li
        {
            float: left;
            text-align: left;
            list-style-type: none;
        }
        .input-group{
            padding-bottom:30px;
            width:100%;
        }
    </style>
</head>

<body>


<div class="container">

    <?php
        if( substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) == "nl"  ){
            ?>
            <h1>Bedankt voor het invullen!</h1>
            <p class="lead">
                Bedankt voor het invullen van de enquete. U mag dit scherm nu sluiten.
            </p>
            <?
        } else{
            ?>
            <h1>Thank you!</h1>
            <p class="lead">
                Thank you for filling out this survey. You can not close this window.
            </p>
        <?
        }
    ?>


    <br />
    <br /><br />

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</body>
</html>
