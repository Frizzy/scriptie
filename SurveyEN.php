<?php
    if( !isset($_GET['key']) ){
        $_GET['key'] = null;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Survey (EN)</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
        .likert ul
        {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        .likert li
        {
            float: left;
            text-align: left;
            list-style-type: none;
        }
        .input-group{
            padding-bottom:30px;
            width:100%;
        }
    </style>
</head>

<body>


<div class="container">

    <h1>Questionnaire</h1>
    <p class="lead">
        You were one bad-ass player! Now please be my superhero and answer the questions below.<br>
        You'll be done in under a minute.
    </p>

    <form method="post" action="Survey.php">

        <input type="hidden" name="key" value="<?php echo $_GET['key'];?>" />
        <input type="hidden" name="lang" value="en" />

        <h3>Questions</h3>

        <h4>Collaboration</h4>

        <div class="input-group">
            <label>How important do you think collaboration was to be able to get a high score (= fast time)? <i>required</i></label>
            <select class="form-control" required name="importance-collaboration">
                <option value="">Please Select</option>
                <option value="very-important">Very Important</option>
                <option value="important">Important</option>
                <option value="moderately-imporant">Moderately Important</option>
                <option value="of-little-importance">Of Little Importance</option>
                <option value="unimportant">Unimportant</option>
            </select>
        </div>

        <div class="input-group">
            <label>How well do you think you collaborated with the other player?  <i>required</i></label>
            <select class="form-control" required name="successfulness-collaboration">
                <option value="">Please Select</option>
                <option value="very-good">Very Good</option>
                <option value="good">Good</option>
                <option value="limited">Limited</option>
                <option value="poor">Poor</option>
                <option value="very-poor">Very Poor</option>
            </select>
        </div>

        <div class="input-group">
            <label>Could you, in a few words, describe your strategy in solving this puzzle?</label>
            <textarea class="form-control" name="strategy" rows="4"></textarea>
        </div>

        <div class="input-group">
            <label>How important where the provided digital tools in achieving this collaboration?  <i>required</i></label>
            <select class="form-control" required name="digital-tools-collaboration">
                <option value="">Please Select</option>
                <option value="very-important">Very Important</option>
                <option value="important">Important</option>
                <option value="moderately-important">Moderately Important</option>
                <option value="of-little-important">Of Little Importance</option>
                <option value="unimportant">Unimportant</option>
            </select>
        </div>

        <div class="input-group">
            <label>What other digital tool would you have liked to see available for improving collaboration?</label>
            <textarea class="form-control" rows="4" name="other-digital-tools"></textarea>
        </div>

        <h4>Demographics</h4>
        <p>
            Please note these answers will be published completely anonymously, and are used to make sure the experiment
            has a nice distibution over all kind of people. All questions under Demographics are optional, but I request you
            to answer them anyway.
        </p>

        <div class="input-group">
            <label>What is your age?</label>
            <select class="form-control" name="age">
                <option value="">Please Select</option>
                <option value="0-18">0-18</option>
                <option value="18-30">18-30</option>
                <option value="30-40">30-40</option>
                <option value="40-60">40-60</option>
                <option value="60">60+</option>
            </select>
        </div>

        <div class="input-group">
            <label>What is your gender?</label>
            <select class="form-control" name="gender">
                <option value="">Please Select</option>
                <option value="m">Male</option>
                <option value="f">Female</option>
                <option value="o">Other</option>
            </select>
        </div>

        <div class="input-group">
            <label>How experienced are you with using computers?</label>
            <select class="form-control" name="computer-experience">
                <option value="">Please Select</option>
                <option value="very-experienced">Very Experienced</option>
                <option value="experienced">Experienced</option>
                <option value="limited-experience">Limited experience</option>
                <option value="very-limited-experience">Very limited experience</option>
                <option value="no-experience">No experience</option>
            </select>
        </div>

        <button type="submit" class="btn btn-lg btn-success">Send!</button>

    </form>

    <br />
    <br /><br />

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</body>
</html>
