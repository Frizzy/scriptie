<?php
    if( !isset($_GET['key']) ){
        $_GET['key'] = null;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Survey (NL)</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
        .likert ul
        {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        .likert li
        {
            float: left;
            text-align: left;
            list-style-type: none;
        }
        .input-group{
            padding-bottom:30px;
            width:100%;
        }
    </style>
</head>

<body>


<div class="container">

    <h1>Enquete</h1>
    <p class="lead">
        Wat heeft u geweldig gespeeld! Zou u nu misschien mijn korte enquete willen invullen?<br />
        Het zijn maar enkele vragen.
    </p>

    <form method="post">

        <input type="hidden" name="key" value="<?php echo $_GET['key'];?>" />
        <input type="hidden" name="lang" value="nl" />

        <h3>Vragen</h3>

        <h4>Samenwerking</h4>

        <div class="input-group">
            <label>Hoe belangrijk  denkt u dat samenwerken was om een hoge score (= snelle tijd) te behalen? <i>verplicht</i></label>
            <select class="form-control" required>
                <option value="">Maak uw keuze</option>
                <option value="very-important">Zeer belangrijk</option>
                <option value="important">Belangrijk</option>
                <option>Enigszinds Belangrijk</option>
                <option>Weinig Belangrijk</option>
                <option>Onbelangrijk</option>
            </select>
        </div>

        <div class="input-group">
            <label>Hoe goed denkt u dat u heeft samengewerkt met uw medespeler? <i>verpicht</i></label>
            <select class="form-control" required>
                <option>Maak uw keuze</option>
                <option>Zeer goed</option>
                <option>Goed</option>
                <option>Kon beter</option>
                <option>Slecht</option>
                <option>Zeer Slecht</option>
            </select>
        </div>

        <div class="input-group">
            <label>Zou u, in een paar woorden, uw strategie voor het oplossen van de puzzel kunnen uitleggen?</label>
            <textarea class="form-control" rows="4"></textarea>
        </div>

        <div class="input-group">
            <label>Hoe belangrijk waren de digitale hulpmiddelen (video, audio, tekenen op scherm etc.) bij het behalen
                van deze samenwerking?  <i>verplicht</i></label>
            <select class="form-control" required>
                <option value="">Maak uw keuze</option>
                <option value="very-important">Zeer belangrijk</option>
                <option>Belangrijk</option>
                <option>Enigszinds Belangrijk</option>
                <option>Weinig Belangrijk</option>
                <option>Onbelangrijk</option>
            </select>
        </div>

        <div class="input-group">
            <label>Welke andere digitale hulpmiddelen zou u graag hebben gezien om een betere samenwerking te kunnen behalen?</label>
            <textarea class="form-control" rows="4"></textarea>
        </div>

        <h4>Demografische gegevens</h4>
        <p>
            Ik maak u er graag op attent dat deze antwoorden geheel anoniem gebruik zullen worden. Deze informatie is nuttig
            voor mij om er zeker van te zijn dat er een goede verdeling is bij de proefpersonen. Alle vragen onder
            demografische gegevens zijn optioneel, maar ik verzoek u om deze toch in te vullen.
        </p>

        <div class="input-group">
            <label>What is uw leeftijd?</label>
            <select class="form-control">
                <option>Maak uw keuze</option>
                <option>0-18</option>
                <option>18-30</option>
                <option>30-40</option>
                <option>40-60</option>
                <option>60-99</option>
            </select>
        </div>

        <div class="input-group">
            <label>What is uw geslacht?</label>
            <select class="form-control">
                <option>Maak uw keuze</option>
                <option>Man</option>
                <option>Vrouw</option>
                <option>Overige</option>
            </select>
        </div>

        <div class="input-group">
            <label>Hoe ervaren zou u zichzelf noemen als het gaat om het gebruik van computers?</label>
            <select class="form-control">
                <option>Maak uw keuze</option>
                <option>Zeer ervaren</option>
                <option>Ervaren</option>
                <option>Enigszinds ervaren</option>
                <option>Weinig ervaren</option>
                <option>Niet ervaren</option>
            </select>
        </div>

        <button type="submit" class="btn btn-lg btn-success">Verzenden!</button>

    </form>

    <br />
    <br /><br />

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</body>
</html>
