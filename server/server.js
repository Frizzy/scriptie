var express = require('express');
var app = express();

console.log( "HI! :)" );

var PeerServer = require('peer').PeerServer;
var server = new PeerServer({port: 9000, path: '/server'});
var connected = [];

server.on('connection', function (id) {
    connected.push(id);
});
server.on('disconnect', function (id) {
    var index = connected.indexOf(id);
    if (index > -1) {
        connected.splice(index, 1);
    }
});
app.get('/connected-people', function (req, res) {
    res.setHeader( "Access-Control-Allow-Origin", "*" );
    return res.json(connected);
});


app.listen(3000);