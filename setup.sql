delimiter $$

CREATE DATABASE `thesis` /*!40100 DEFAULT CHARACTER SET latin1 */$$

delimiter $$

CREATE TABLE `games` (
  `sesId` varchar(45) DEFAULT NULL,
  `playtime` int(11) DEFAULT NULL,
  `clickX` text,
  `clickY` text,
  `clickDrag` text,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playertype` int(2) DEFAULT NULL,
  `drawingEnabled` int(1) DEFAULT NULL,
  `uniqueKey` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1$$


delimiter $$

CREATE TABLE `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(120) DEFAULT NULL,
  `lang` varchar(5) DEFAULT NULL,
  `importance-collaboration` varchar(45) DEFAULT NULL,
  `successfulness-collaboration` varchar(45) DEFAULT NULL,
  `strategy` text,
  `digital-tools-collaboration` varchar(45) DEFAULT NULL,
  `other-digital-tools` text,
  `age` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `computer-experience` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1$$


SELECT * FROM thesis.survey;