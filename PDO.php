<?php
/**
 * For license information; see license.txt
 * @author frankhouweling
 * @date 21-05-14
 * @copyright Fruitbomen.net 2014
 */

$db = new PDO('mysql:host=localhost;dbname=thesis;charset=utf8', 'root', '');

if( !$db ){
    throw new Exception("Could not connect");
}