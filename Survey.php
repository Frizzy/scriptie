<?php
/**
 * For license information; see license.txt
 * @author frankhouweling
 * @date 21-05-14
 * @copyright Fruitbomen.net 2014
 */

require_once "PDO.php";
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$fields = array(
    "key", "lang", "importance-collaboration", "successfulness-collaboration",
    "strategy", "digital-tools-collaboration", "other-digital-tools",
    "age", "gender", "computer-experience"
);

$variables = array_map(function($val){
    return ":" . str_replace("-", "_", $val);
}, $fields);

$data = array();
foreach( $fields as $field ){
    $data[":" . str_replace("-", "_", $field)] = $_POST[$field];
}

$sql = "INSERT INTO survey(`" . implode("`, `", $fields) . "` )
            VALUES(" . implode(", ", $variables) . ")";

$query = $db->prepare($sql);
$query->execute($data);

header( "Location: tnx.php" );