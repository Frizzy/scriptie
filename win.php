<?php
    require_once "PDO.php";

    $query = $db->prepare("INSERT INTO games (sesId, playtime, clickX, clickY, clickDrag, playertype, drawingEnabled, uniqueKey)
                            VALUES(:sesId, :playtime, :clickX, :clickY, :clickDrag, :playertype, :drawingEnabled, :uniqueKey)
                            ");

    $vars = array();
    foreach( $_POST as $key => $val ){
        $vars[":" . $key] = $val;
    }

    $vars[":playtime"] = $vars[":time"];
    $vars[":uniqueKey"] = md5( time() . rand(0,99999999) );
    unset($vars[":time"]);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $res = $query->execute($vars);

    if( substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) == "nl"  ){
        header( "Location: SurveyNL.php?key=" . $vars[":uniqueKey"] );
    } else{
        header( "Location: SurveyEN.php?key=" . $vars[":uniqueKey"] );
    }
?>