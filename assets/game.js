/**
 * Created with JetBrains PhpStorm.
 * User: frankhouweling
 * Date: 27-04-14
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function(){


    function startPlaying( playerType, connection ){


        /*
         * Draw maze
         */
        var maze = $('#maze')[0];
        var mazectx = maze.getContext('2d');

        var image = new Image();
        image.src = 'assets/maze.jpg';
        image.onload = function(){
            mazectx.drawImage(image,0,0);
        };

        if( playerType == 0 ){
            var drawingEnabled = Math.random()<.5; // Random Boolean
            console.log(drawingEnabled);

            if( drawingEnabled ){
                $('#tmpscreen').html( translate("tmpscreen-player-0-withdraw") );
            } else{
                $('#tmpscreen').html( translate("tmpscreen-player-0-withoutdraw") );
            }
        } else{
            $('#tmpscreen').html( translate("tmpscreen-player-1") );
        }

        $('#tmpscreen #close').click(function(){
            $('#tmpscreen').hide();
            if( playerType == 1 ){
                timer();
            }
        });

        /*
         * Hide/show maze..
         */
        if( playerType == 1 ){
            $('#hider').show();
        }

        /*
         * Draw Timer
         */

        function pad(num, size) {
            var s = num+"";
            while (s.length < size) s = "0" + s;
            return s;
        }

        function makeTime(seconds){
            var minutes = Math.floor( seconds / 60 );
            var secondsLeft = seconds - (minutes * 60);
            return pad(minutes,2) + ":" + pad(secondsLeft,2);
        }

        /*
         * Movable puppet..
         */
        var puppet = $('#puppet')[0];
        var puppetctx = puppet.getContext('2d');

        var movingLeft = false;
        var movingUp = false;
        var movingRight = false;
        var movingDown = false;
        var puppetPositionX = 11;
        var puppetPositionY = 11;

        /*
         * Free Drawing
         */
        var freedraw = $('#freedraw')[0];
        var freedrawctx = freedraw.getContext('2d');

        var clickX = new Array();
        var clickY = new Array();
        var clickDrag = new Array();
        var paint;

        function redraw(){
            if( playerType == 0 ){
                connection.send( { drawDataX: clickX, drawDataY : clickY, drawDataDrag : clickDrag } );
            }

            freedrawctx.clearRect(0, 0, freedrawctx.canvas.width, freedrawctx.canvas.height); // Clears the canvas

            freedrawctx.strokeStyle = "#df4b26";
            freedrawctx.lineJoin = "round";
            freedrawctx.lineWidth = 4;

            for(var i=0; i < clickX.length; i++) {
                freedrawctx.beginPath();
                if(clickDrag[i] && i){
                    freedrawctx.moveTo(clickX[i-1], clickY[i-1]);
                }else{
                    freedrawctx.moveTo(clickX[i]-1, clickY[i]);
                }
                freedrawctx.lineTo(clickX[i], clickY[i]);
                freedrawctx.closePath();
                freedrawctx.stroke();
            }
        }

        /*
         * Helpers
         */

        var playingTime = 0;
        var hasAlreadyWon = false;

        function rgbToHex(r, g, b) {
            if (r > 255 || g > 255 || b > 255)
                throw "Invalid color component";
            return ((r << 16) | (g << 8) | b).toString(16);
        }

        function drawPuppet(x, y){
            puppetctx.clearRect ( 0 , 0 , 400 , 400 );
            puppetctx.beginPath();
            puppetctx.arc(x,y,5,0,2*Math.PI);
            puppetctx.fillStyle = 'green';
            puppetctx.fill();
            puppetctx.lineWidth = 1;
            puppetctx.strokeStyle = '#003300';
            puppetctx.stroke();
        }

        function move(){

            if( puppetPositionY >= 325 && puppetPositionX >= 385 && playerType == 1 ){
                if( !hasAlreadyWon ){
                    connection.send( { win : true, winTime : playingTime });
                    $('input[name="time"]').val( playingTime );
                    $('input[name="playertype"]').val(playerType);
                    $('input[name="sesId"]').val(connection.id);
                    $('input[name="clickX"').val(JSON.stringify(clickX));
                    $('input[name="clickY"]').val(JSON.stringify(clickY));
                    $('input[name="clickDrag"]').val(JSON.stringify(clickDrag));
                    $('#winform').submit();
                    hasAlreadyWon = true;
                }
            }

            /* Moving right */
            var p = mazectx.getImageData(puppetPositionX + 4, puppetPositionY, 1, 1).data;
            if( movingRight && ("000000" + rgbToHex(p[0], p[1], p[2])).slice(-6) == "ffffff" ){
                puppetPositionX += 2;
            }

            /* Moving left */
            var p = mazectx.getImageData(puppetPositionX - 4, puppetPositionY, 1, 1).data;
            if( movingLeft && ("000000" + rgbToHex(p[0], p[1], p[2])).slice(-6) == "ffffff" ){
                puppetPositionX -= 2;
            }

            /* Moving down */
            var p = mazectx.getImageData(puppetPositionX, puppetPositionY + 4, 1, 1).data;
            if( movingDown && ("000000" + rgbToHex(p[0], p[1], p[2])).slice(-6) == "ffffff" ){
                puppetPositionY += 2;
            }

            /* Moving up */
            var p = mazectx.getImageData(puppetPositionX, puppetPositionY - 4, 1, 1).data;
            if( movingUp && ("000000" + rgbToHex(p[0], p[1], p[2])).slice(-6) == "ffffff" ){
                puppetPositionY -= 2;
            }

            connection.send( {x : puppetPositionX, y : puppetPositionY, currentTime : playingTime} );

            drawPuppet(puppetPositionX, puppetPositionY);

            setTimeout(function(){
                move();
            }, 6);
        }

        if( playerType == 1 ){
            connection.on('data', function(data) {
                if( data.drawDataX ){
                    clickX = data.drawDataX;
                    clickY = data.drawDataY;
                    clickDrag = data.drawDataDrag;
                    redraw();
                }
            });

            function timer(){
                playingTime += 1;
                $('#timer').html( makeTime(playingTime) );
                setTimeout(function(){
                    timer();
                }, 1000);
            }

            $("body").keydown(function(e) {
                if(e.keyCode == 37) { // left
                    movingLeft = true;
                }
                else if(e.keyCode == 39) { // right
                    movingRight = true;
                }
                else if(e.keyCode == 40) { // down
                    movingDown = true;
                }
                else if(e.keyCode == 38) { // up
                    movingUp = true;
                }
            });

            $("body").keyup(function(e) {
                if(e.keyCode == 37) { // left
                    movingLeft = false;
                }
                else if(e.keyCode == 39) { // right
                    movingRight = false;
                }
                else if(e.keyCode == 40) { // down
                    movingDown = false;
                }
                else if(e.keyCode == 38) { // up
                    movingUp = false;
                }
            });
        } else{
            connection.on('data', function(data) {
                if( data.win ){
                    $('input[name="time"]').val( data.winTime );
                    $('input[name="playertype"]').val(playerType);
                    $('input[name="sesId"]').val(connection.id);
                    $('input[name="clickX"').val(JSON.stringify(clickX));
                    $('input[name="clickY"]').val(JSON.stringify(clickY));
                    $('input[name="clickDrag"]').val(JSON.stringify(clickDrag));
                    if( drawingEnabled == true ){
                        $('input[name="drawingEnabled"]').val("1");
                    } else{
                        $('input[name="drawingEnabled"]').val("0");
                    }
                    $('#winform').submit();
                } else{
                    $('#timer').html( makeTime( data.currentTime ) );
                    puppetPositionX = data.x;
                    puppetPositionY = data.y;
                }
            });

            $('canvas').css('cursor', 'crosshair');

            $('#freedraw').mousedown(function(e){
                var mouseX = e.pageX - this.offsetLeft;
                var mouseY = e.pageY - this.offsetTop;

                paint = true;
                addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
                redraw();
            });

            $('#freedraw').mousemove(function(e){
                if(paint){
                    addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
                    redraw();
                }
            });

            $('#freedraw').mouseup(function(e){
                paint = false;
            });

            $('#freedraw').mouseleave(function(e){
                paint = false;
            });

            var seenAlert = false;

            function addClick(x, y, dragging)
            {
                if( drawingEnabled == true ){
                    if( $('#timer').html() != "00:00" ){
                        clickX.push(x);
                        clickY.push(y);
                        clickDrag.push(dragging);
                    } else if( !seenAlert ){
                        seenAlert = true;
                        alert( translate("you-cannot-draw") );
                    }
                }
            }

        }

        drawPuppet(puppetPositionX, puppetPositionY);
        move();
    }

    /*
     * PeerJS
     */
    var myId = 0;
    var playerType = null;
    var connection = null;

    var startedPlaying = false;

    var peer = new Peer({ host : "localhost", port : 9000, path : "/server", debug : 3 });

    /*
     * Video Chatting
     */
    // Compatibility shim
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

    // Receiving a call
    peer.on('call', function(call){
        // Answer the call automatically (instead of prompting user) for demo purposes
        call.answer(window.localStream);
        step3(call);
    });
    peer.on('error', function(err){
        alert(err.message);
        // Return to step 2 if error occurs
        step2();
    });

    // Click handlers setup
    $(function(){
        $('#make-call').click(function(){
            // Initiate a call!
            var call = peer.call($('#callto-id').val(), window.localStream);

            step3(call);
        });

        $('#end-call').click(function(){
            window.existingCall.close();
            step2();
        });

        // Retry if getUserMedia fails
        $('#step1-retry').click(function(){
            $('#step1-error').hide();
            step1();
        });

    });

    function step1 () {
        // Get audio/video stream

    }

    function step2 () {
        $('#step1, #step3').hide();
        $('#step2').show();
    }

    function step3 (call) {
        // Hang up on an existing call if present
        if (window.existingCall) {
            window.existingCall.close();
        }

        // Wait for stream on the call, then set peer video display
        call.on('stream', function(stream){
            $('#their-video').prop('src', URL.createObjectURL(stream));
        });

        // UI stuff
        window.existingCall = call;
        $('#their-id').text(call.peer);
        call.on('close', step2);
        $('#step1, #step2').hide();
        $('#step3').show();
    }

    peer.on('open', function(id) {
        $('#videoaccess').show();
        navigator.getUserMedia({audio: true, video: true}, function(stream){
            $('#videoaccess').hide();
            // Set your video displays
            $('#my-video').prop('src', URL.createObjectURL(stream));

            window.localStream = stream;
            myId = id;

            $.ajax({
                url: "http://localhost:3000/connected-people"
            })
            .done(function( data ) {
                if( data.length != 1 ){
                    playerType = 0;
                    connection = peer.connect(data[0]);
                    var call = peer.call(data[0], window.localStream);
                    step3(call);
                    connectionOpened();
                }
            });

            step2();
        }, function(){ $('#step1-error').show(); });
    });

    peer.on('connection', function(conn) {
        connection = conn;
        playerType = 1;

        connectionOpened();
    });

    function connectionOpened(){
        connection.on('open', function(){

            // Receive messages
            connection.on('data', function(data) {
                if( !startedPlaying ){
                    console.log('Received ', data);
                    startPlaying( playerType, connection );
                    startedPlaying = true;
                }
            });

            // Send messages
            connection.send('Handshake :)');
        });
        connection.on('close', function(){
            console.log("Hoi?");
            location.reload();
        });
    }

});