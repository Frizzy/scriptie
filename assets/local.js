/**
 * Created with JetBrains PhpStorm.
 * User: frankhouweling
 * Date: 19-05-14
 * Time: 22:29
 * To change this template use File | Settings | File Templates.
 */

var translations = new Array();

translations['en'] = new Array();
translations['nl'] = new Array();

/*
 * English
 */

translations['en']['tmpscreen-player-0-withdraw'] = '<p><strong>Welcome player!</strong><br />This looks like a normal maze game; but it ' +
    'is not!<br /> You play this game together with another person. You, and the other player, have other ' +
    'limitations. You are able to see the maze, but ' +
    'unable to move the character over the screen to the end of the maze. The other player is unable to ' +
    'see the maze, but able to move the character.<br />It is your task to move the character to the endpoint ' +
    'of the maze, and do this as quickly as possible.<br />This other person will be able to see and hear you ' +
    'over the videoconferencing screen on the bottom-right of your browser window. You will be able to give him ' +
    'extra directions by drawing on the screen with your cursor (the mouse thingie). The other person is ' +
    'able to see your drawings.' +
    '<br />After the game, a few multiple choice questions are displayed, and it would help me with my ' +
    'bachelor thesis if you took the time to answer them.<br /><br /> Good luck!<br /><br />' +
    '<i>p.s. the time starts when the other player closes this window</i></p><a id="close">Start the game</a>';

translations['en']['tmpscreen-player-0-withoutdraw'] = '<p><strong>Welcome player!</strong><br />This looks like a normal maze game; but it ' +
    'is not!<br /> You play this game together with another person. You, and the other player, have other ' +
    'limitations. You are able to see the maze, but ' +
    'unable to move the character over the screen to the end of the maze. The other player is unable to ' +
    'see the maze, but able to move the character.<br />It is your task to move the character to the endpoint ' +
    'of the maze, and do this as quickly as possible.<br />This other person will be able to see and hear you ' +
    'over the videoconferencing screen on the bottom-right of your browser window.' +
    '<br />After the game, a few multiple choice questions are displayed, and it would help me with my ' +
    'bachelor thesis if you took the time to answer them.<br /><br /> Good luck!<br /><br />' +
    '<i>p.s. the time starts when the other player closes this window</i></p><a id="close">Start the game</a>';

translations['en']['welcome-screen'] = '<h3>Welcome to the Collaborative Maze!</h3>' +
'    <p>' +
'    Welcome to this collaborative maze. In this maze-game you will have to collaborate with another player to' +
    'get through the maze because only one player can control the character, and only the other can see the maze' +
    'itself.' +
    '<br />' +
    '  <br />' +
    '  <strong>Share this link to invite another player, and you can play this game together!</strong>' +
    ' <br />' +
    '  <input type="text" value="http://thesis.ga/" />' +
    '   <br />' +
    '  <br />' +
    '  <i>Just in a game? The other player has most likely disconnected.</i>' +
    '</p>';

translations['en']['audio-video-sharing'] = '<h3>Please allow video and audio sharing</h3>' +
    '    <p>' +
    '    Please note that your video and audio will be shared with another, randomly chosen person.' +
    '</p>' +
    '    <p>' +
    '    You can accept sharing with the "Accept" button in the bar above.' +
'    </p>';

translations['en']['tmpscreen-player-1'] = '<p><strong>Welcome player!</strong><br />This looks like a normal maze game; but it ' +
    'is not!<br /> You play this game together with another person. You, and the other player, have other ' +
    'limitations. You are unable to see the maze, but able to move the character over the screen to the end' +
    ' of the maze. The other player is able to see the maze, but unable to move the character.<br />' +
    ' It is your task to move the character to the endpoint of the maze, and do this as quickly as possible.<br />' +
    'This other person will be able to see and hear you over the videoconferencing screen on the bottom-right of ' +
    'your browser window. He will also be able to give you extra directions with drawings on your screen.' +
    '<br />You can move the character around the maze with the arrow keys on your keyboard.<br />' +
    ' After the game, a few multiple choice questions are displayed, and it would help me with my ' +
    'bachelor thesis if you took the time to answer them.<br /><br /> Good luck!<br /><br />' +
    '<i>p.s. the time starts when you close this window</i></p><a id="close">Start the game</a>';

translations['en']['you-cannot-draw'] = "You cannot draw until the other person starts the game. Sorry!\n\n You can start drawing when the clock " +
    "in the top-right of your screen starts ticking.";

/*
 * Dutch
 */

translations['nl']['audio-video-sharing'] = '<h3>Sta a.u.b. audio en videodeling toe</h3>' +
    '    <p>' +
    '    Houd u er a.u.b. rekening mee dat uw audio en video gedeeld zullen worden met een willekeurig persoon.' +
    '</p>' +
    '    <p>' +
    '    Om het delen toe te staan, klik rechtsbovenin op "Toestaan".' +
    '    </p><img src="assets/accept-nl.png" />';

translations['nl']['welcome-screen'] = '<h3>Welkom bij het collaboratieve doolhof!</h3>' +
    '    <p>' +
    'Welkom in het collaboratieve doolhof. In dit spel moet u samenwerken met een andere speler om naar het einde van' +
    'het doolhof te komen. Dit, aangezien &eacute;&eacute;n persoon de speler kan bedienen, en &eacute;&eacute;n persoon' +
    'het doolhof ziet.' +
    '<br />' +
    '  <br />' +
    '  <strong>Deel deze link met een andere speler, en speel samen!</strong>' +
    ' <br />' +
    '  <input type="text" value="http://thesis.ga/" />' +
    '   <br />' +
    '  <br />' +
    '  <i>U was net nog aan het spelen? Dan heeft de andere speler het spel waarschijnlijk weggeklikt.</i>' +
    '</p>';

translations['nl']['you-cannot-draw'] = "Sorry! U kunt niet tekenen voordat de andere persoon het spel start. \n\n" +
    "Dit is wanneer de klok linksbovenin begint te lopen."

translations['nl']['tmpscreen-player-0-withdraw'] = '<p><strong>Welkom!</strong><br />Dit ziet er misschien uit als een normaal doolhof, maar dat is het niet!' +
    'U speelt dit spel samen met een andere persoon. U, en de andere persoon, hebben een andere functie.' +
    'U kunt het doolhof zien, maar kunt het poppetje niet naar het einde van het doolhof bewegen.' +
    'De andere persoon kan het poppetje wel bewegen, maar is niet in staat om het doolhof te zien.<br />' +
    'Het is jullie doel om zo snel mogelijk naar het einde van het doolhof te komen.<br />' +
    'U kunt de andere persoon naar het einde van het doolhof leiden door hem instructies te geven via de video- en' +
    'audiochat, of door (met uw muis) op het scherm te tekenen.' +
    '<br />Na het spel worden enkele vragen getoont, en het is voor mijn onderzoek heel belangrijk' +
    'dat u deze beantwoord..<br /><br /> Veel success!!<br /><br />' +
    '<i>p.s. de tijd begint te lopen als de andere speler het spel start</i></p><a id="close">Start het spel!</a>';

translations['nl']['tmpscreen-player-0-withoutdraw'] = '<p><strong>Welkom!</strong><br />Dit ziet er misschien uit als een normaal doolhof, maar dat is het niet!' +
    'U speelt dit spel samen met een andere persoon. U, en de andere persoon, hebben een andere functie.' +
    'U kunt het doolhof zien, maar kunt het poppetje niet naar het einde van het doolhof bewegen.' +
    'De andere persoon kan het poppetje wel bewegen, maar is niet in staat om het doolhof te zien.<br />' +
    'Het is jullie doel om zo snel mogelijk naar het einde van het doolhof te komen.<br />' +
    'U kunt de andere persoon naar het einde van het doolhof leiden door hem instructies te geven via de video- en' +
    'audiochat.' +
    '<br />Na het spel worden enkele vragen getoont, en het is voor mijn onderzoek heel belangrijk' +
    'dat u deze beantwoord..<br /><br /> Veel success!!<br /><br />' +
    '<i>p.s. de tijd begint te lopen als de andere speler het spel start</i></p><a id="close">Start het spel!</a>';

translations['nl']['tmpscreen-player-1'] = '<p><strong>Welkom!</strong><br />Dit ziet er misschien uit als een normaal doolhof, maar dat is het niet!' +
    'U speelt dit spel samen met een andere persoon. U, en de andere persoon, hebben een andere functie.' +
    'U kunt het doolhof niet zien; deze ziet alleen de andere speler. U daarintegen kunt het poppetje bewegen' +
    '; wat de andere speler weer niet kan.' +
    'Het is jullie doel om zo snel mogelijk naar het einde van het doolhof te komen.<br />' +
    'De andere speler zal o.a. via audio- en videochat proberen u naar het einde van het doolhof te leiden.' +
    '<br />Na het spel worden enkele vragen getoont, en het is voor mijn onderzoek heel belangrijk' +
    'dat u deze beantwoord..<br /><br /> Veel success!!<br /><br />' +
    '<i>p.s. de tijd begint te lopen als u op Start het spel! klikt.</i></p><a id="close">Start het spel!</a>';

function translate(key){
    var lang = "en";

    if( ( window.navigator.userLanguage || window.navigator.language ) == "nl" ){
        lang = "nl";
    }

    return translations[lang][key];
}